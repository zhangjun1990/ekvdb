/*
 * Copyright (c) 2022 Hong Jiahua
 * https://gitee.com/arrco/ekvdb
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef __EKVDB_H__
#define __EKVDB_H__

/************************************************************************/
/*                                                                      */
/************************************************************************/
typedef struct
{
    uint32_t pos;
    uint32_t next;
} ekvdb_iterator_t;

/************************************************************************/
/*                                                                      */
/************************************************************************/
/**
  * @brief  冗余统计支持        1：开启 0：关闭
  */
#define EKVDB_REDUNDANT_SUPPORT     1

/**
  * @brief  自动清除冗余支持    1：开启 0：关闭
  */
#define EKVDB_AUTO_COMPACT_SUPPORT  1

/**
  * @brief  自动扩展容量支持    1：开启 0：关闭
  */
#define EKVDB_AUTO_EXTEND_SUPPORT   1

/**
  * @brief  自动计数支持        1：开启 0：关闭
  */
#define EKVDB_AUTO_COUNT_SUPPORT    1

#if EKVDB_AUTO_COMPACT_SUPPORT
#undef  EKVDB_REDUNDANT_SUPPORT
#define EKVDB_REDUNDANT_SUPPORT     1
#endif
/************************************************************************/
/*                                                                      */
/************************************************************************/
/*创建数据库*/
int ekvdb_create(char* path, uint32_t len, uint32_t threshold);
/*读数据库记录*/
int ekvdb_read(char* path, char* key, void* val, uint32_t vallen);
/*写数据库数据*/
int ekvdb_write(char* path, char* key, void* val, uint32_t vallen);
/*删除数据库记录*/
int ekvdb_delete(char* path, char* key);
/*清除数据库的冗余数据*/
int ekvdb_compact(char* path);
/*扩展数据库容量*/
int ekvdb_extend(char* path);
/*快速扩展数据库容量*/
int ekvdb_quick_extend(char* path);
/*重置数据库*/
int ekvdb_reset(char* path);
/*清空数据库*/
void ekvdb_clear(char* path);
/*数据库计数*/
int ekvdb_count(char* path);
/*数据库容量*/
int ekvdb_capacity(char* path);
/*数据库数据冗余*/
int ekvdb_redundant(char* path);
/*数据库迭代初始化*/
int ekvdb_iterator_init(char* path, ekvdb_iterator_t* iterator);
/*数据库迭代获取下一个键值对*/
int ekvdb_iterator_next(char* path, ekvdb_iterator_t* iterator, char* key, uint32_t keymaxlen, void* val, uint32_t valmaxlen);

#endif