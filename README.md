# ekvdb

## 介绍

ekvdb是一个基于文件的简易键值数据库。完全由C语言实现，只使用标准函数库和FILE I/O 函数，可以直接在含有文件系统的平台中直接使用。

ekvdb 的 key 必须是一个字符串，value 则没有任何限制。文件大小限制在 $2^{32}$ 字节。

支持添加，更新，读取，删除，迭代等功能，迭代时没有明确的键顺序。

所有内容都保存在文件中，不进行读写操作时没有任何内存占用。

创建数据库会生成一个索引文件和一个数据文件，数据库是通过索引文件的索引表来管理数据。

<img src=".\resources\Index.jpg" alt="Index" style="zoom:25%;" />



<img src=".\resources\Data.jpg" alt="Data" style="zoom:25%;" />

索引文件是根据创建数据库时传入的大小创建的一个固定大小的哈希表来作为索引表，并通过链地址法解决哈希冲突。

数据文件中存储键值对，以日志形式存储，每一个数据的更新都是直接追加一条更新记录来完成，保证了顺序写，由索引表来维护数据在日志中的位置。

由于以日志形式存储，所以在更新与删除数据后就会产生冗余存储，ekvdb 支持自动/手动清除冗余数据。

在数据库容量使用完之后，支持自动/手动扩展容量。

由于是一个简易的数据库，如果数据库损坏，没有任何恢复机制，也没有内置线程安全，没有字节序转换处理等。

## 使用说明

只需要包含 `ekvdb.c` 和 `ekvdb.h` 两个文件即可使用。

## 接口函数

提供以下接口用于数据库的使用：

```c
/*创建数据库*/
int ekvdb_create(char* path, uint32_t len, uint32_t threshold);
/*读数据库记录*/
int ekvdb_read(char* path, char* key, void* val, uint32_t vallen);
/*写数据库数据*/
int ekvdb_write(char* path, char* key, void* val, uint32_t vallen);
/*删除数据库记录*/
int ekvdb_delete(char* path, char* key);
/*清除数据库的冗余数据*/
int ekvdb_compact(char* path);
/*扩展数据库容量*/
int ekvdb_extend(char* path);
/*重置数据库*/
int ekvdb_reset(char* path);
/*清空数据库*/
void ekvdb_clear(char* path);
/*数据库计数*/
int ekvdb_count(char* path);
/*数据库容量*/
int ekvdb_capacity(char* path);
/*数据库数据冗余*/
int ekvdb_redundant(char* path);
/*数据库迭代初始化*/
int ekvdb_iterator_init(char* path, ekvdb_iterator_t* iterator);
/*数据库迭代获取下一个键值对*/
int ekvdb_iterator_next(char* path, ekvdb_iterator_t* iterator, char* key, uint32_t keymaxlen, void* val, uint32_t valmaxlen);
```

## 接口使用说明

### 1.数据库初始化

调用接口`ekvdb_create`初始化集合。

```c
/*创建数据库*/
int ekvdb_create(char* path, uint32_t len, uint32_t threshold);
```

函数参数含义：

| 参数      | 含义                                                |
| --------- | --------------------------------------------------- |
| path      | 路径                                                |
| len       | 数据库存储键值对的数量                              |
| threshold | 自动清除冗余数据的阈值，不使用对应功能时可以设置为0 |

函数返回值：

| 返回值 | 含义 |
| ------ | ---- |
| 0      | 成功 |
| -1     | 失败 |

### 2.读数据库

调用接口`ekvdb_read`读数据库记录。

```c
/*读数据库记录*/
int ekvdb_read(char* path, char* key, void* val, uint32_t vallen);
```

函数参数含义：

| 参数   | 含义     |
| ------ | -------- |
| path   | 路径     |
| key    | 键       |
| val    | 值       |
| vallen | 值的长度 |

函数返回值：

| 返回值 | 含义             |
| ------ | ---------------- |
| >=0    | 成功读取的值长度 |
| -1     | 失败             |

### 3.写数据库

调用接口`ekvdb_write`写数据库数据。

```c
/*写数据库数据*/
int ekvdb_write(char* path, char* key, void* val, uint32_t vallen);
```

函数参数含义：

| 参数   | 含义     |
| ------ | -------- |
| path   | 路径     |
| key    | 键       |
| val    | 值       |
| vallen | 值的长度 |

函数返回值：

| 返回值 | 含义             |
| ------ | ---------------- |
| >=0    | 成功写入的值长度 |
| -1     | 失败             |

### 4.删除记录

调用接口`ekvdb_delete`删除数据库记录。

```c
/*删除数据库记录*/
int ekvdb_delete(char* path, char* key);
```

函数参数含义：

| 参数 | 含义 |
| ---- | ---- |
| path | 路径 |
| key  | 键   |

函数返回值：

| 返回值 | 含义 |
| ------ | ---- |
| 0      | 成功 |
| -1     | 失败 |

### 5.删除数据库

调用接口`ekvdb_clear`清空删除整个数据库。

```c
/*清空数据库*/
void ekvdb_clear(char* path);
```

函数参数含义：

| 参数 | 含义 |
| ---- | ---- |
| path | 路径 |

函数返回值：

| 返回值 | 含义 |
| ------ | ---- |
| 无     | 无   |

### 6.重置数据库

调用接口`ekvdb_reset`重置数据库，数据记录被清空。

```c
/*重置数据库*/
int ekvdb_reset(char* path);
```

函数参数含义：

| 参数 | 含义 |
| ---- | ---- |
| path | 路径 |

函数返回值：

| 返回值 | 含义 |
| ------ | ---- |
| 0      | 成功 |
| -1     | 失败 |

### 7.数据库计数

调用接口`ekvdb_count`查看放入数据库的键值对数量，需要打开配置项`EKVDB_AUTO_COUNT_SUPPORT`支持功能。

```c
/*数据库计数*/
int ekvdb_count(char* path);
```

函数参数含义：

| 参数 | 含义 |
| ---- | ---- |
| path | 路径 |

函数返回值：

| 返回值 | 含义                   |
| ------ | ---------------------- |
| >=0    | 数据库内存储的数据数量 |
| -1     | 失败                   |

### 8.数据库容量

调用接口`ekvdb_capacity`查看数据库能存放的最少键值对数量，实际能存放的数据数量大于等于该容量值。

```c
/*数据库容量*/
int ekvdb_capacity(char* path);
```

函数参数含义：

| 参数 | 含义 |
| ---- | ---- |
| path | 路径 |

函数返回值：

| 返回值 | 含义         |
| ------ | ------------ |
| >=0    | 数据库的容量 |
| -1     | 失败         |

### 9.数据迭代

定义一个`ekvdb_iterator_t`类型的迭代器，调用接口`ekvdb_iterator_init`进行迭代初始化，然后调用`ekvdb_iterator_next`进行数据迭代。

```c
/*数据库迭代初始化*/
int ekvdb_iterator_init(char* path, ekvdb_iterator_t* iterator);
/*数据库迭代获取下一个键值对*/
int ekvdb_iterator_next(char* path, ekvdb_iterator_t* iterator, char* key, uint32_t keymaxlen, void* val, uint32_t valmaxlen);
```

`ekvdb_iterator_init`函数参数含义：

| 参数     | 含义   |
| -------- | ------ |
| path     | 路径   |
| iterator | 迭代器 |

`ekvdb_iterator_init`函数返回值：

| 返回值 | 含义 |
| ------ | ---- |
| 0      | 成功 |
| -1     | 失败 |

`ekvdb_iterator_next`函数参数含义：

| 参数      | 含义                                                         |
| --------- | ------------------------------------------------------------ |
| path      | 路径                                                         |
| iterator  | 迭代器                                                       |
| key       | 键的缓冲区，用于存放下一个键，如果键的缓冲区大小无法存放下键，则最后一个Byte的值不为0 |
| keymaxlen | 键的缓冲区大小                                               |
| val       | 值的缓冲区，用于存放下一个值                                 |
| valmaxlen | 值的缓冲区大小                                               |

`ekvdb_iterator_next`函数返回值：

| 返回值 | 含义         |
| ------ | ------------ |
| >=0    | 获取值的大小 |
| -1     | 失败         |

使用示例：

```c
//定义一个用于迭代器
ekvdb_iterator_t iterator;
//迭代器初始化
ekvdb_iterator_init(EKVDB_TEST_PATH, &iterator);
//循环迭代数据
while(1) {
    ret = ekvdb_iterator_next(EKVDB_TEST_PATH, &iterator, keybuf, sizeof(keybuf), valbuf, sizeof(valbuf));
    if(ret < 0)
        break;
}
```

### 10.清除冗余数据

调用接口`ekvdb_compact`清除数据库的冗余数据。

```c
/*清除数据库的冗余数据*/
int ekvdb_compact(char* path);
```

函数参数含义：

| 参数 | 含义 |
| ---- | ---- |
| path | 路径 |

函数返回值：

| 返回值 | 含义 |
| ------ | ---- |
| 0      | 成功 |
| -1     | 失败 |

### 11.冗余数据量

调用接口`ekvdb_redundant`查看数据库的冗余数据量，需要打开配置项`EKVDB_REDUNDANT_SUPPORT`支持功能。

```c
/*数据库数据冗余*/
int ekvdb_redundant(char* path);
```

函数参数含义：

| 参数 | 含义 |
| ---- | ---- |
| path | 路径 |

函数返回值：

| 返回值 | 含义         |
| ------ | ------------ |
| >=0    | 数据库的冗余 |
| -1     | 失败         |

### 12.扩展数据库容量

调用接口`ekvdb_extend`扩展数据库容量。

```c
/*扩展数据库容量*/
int ekvdb_extend(char* path);
```

函数参数含义：

| 参数 | 含义 |
| ---- | ---- |
| path | 路径 |

函数返回值：

| 返回值 | 含义 |
| ------ | ---- |
| >=0    | 成功 |
| -1     | 失败 |

## 配置使用说明

### 1.冗余统计支持

配置项`EKVDB_REDUNDANT_SUPPORT`用于支持数据库的冗余数据统计功能。当打开该配置时，接口`ekvdb_redundant`才可以使用。开启该配置会增加数据库内部对文件的读写操作。

### 2.自动清除冗余支持

配置项`EKVDB_AUTO_COMPACT_SUPPORT`用于开启自动清除冗余数据的功能。打开该配置后，当数据冗余大于阈值时，会触发自动清除冗余的操作。阈值`threshold`在数据库初始化`ekvdb_create`的时候设置。注意，清除冗余数据的操作是一个耗时操作，可能会导致某次写操作时间较长。

### 3.自动扩展容量支持

配置项`EKVDB_AUTO_EXTEND_SUPPORT`用于开启自动扩展数据库容量的功能。打开该配置后，当写数据时，如果容量不足，将触发自动扩展数据库容量的操作。如果数据库需要写入超出其容量的数据量时，推荐手动调用`ekvdb_extend`接口来进行扩展容量。

### 4.自动计数支持

配置项`EKVDB_AUTO_COUNT_SUPPORT`用于开启自动统计数据库数据数量的功能。当打开该配置时，接口`ekvdb_count`才可以使用。开启该配置会增加数据库内部对文件的读写操作。

## 使用示例

基础数据库读写用法：

```c
int main(int argc, char* argv[])
{
#define EKVDB_TEST_PATH     "./"
    
    char name[128];
    int ret;
    
    //创建数据库
    ekvdb_create(EKVDB_TEST_PATH, 16, 0);
    
    //写数据
    for(int i = 0; i < 16; i++) {
        sprintf(name, "%d", i);
        printf("write i %d key %s val %d\n", i, name, i);
        ret = ekvdb_write(EKVDB_TEST_PATH, name, &i, sizeof(int));
        if(ret <= 0)
            printf("ret %d %d\n", ret, i);
    }
    printf("\n");
    
    //读数据
    int readval;
    for(int i = 0; i < 16; i++) {
        sprintf(name, "%d", i);
        ret = ekvdb_read(EKVDB_TEST_PATH, name, &readval, sizeof(int));
        if(ret > 0)
            printf("read i %d key %s val %d\n", i, name, readval);
    }
    printf("\n");
    
    //清空删除数据库
    ekvdb_clear(EKVDB_TEST_PATH);
    
    return 0;
}
```

运行结果如下：

```
write i 0 key 0 val 0
write i 1 key 1 val 1
write i 2 key 2 val 2
write i 3 key 3 val 3
write i 4 key 4 val 4
write i 5 key 5 val 5
write i 6 key 6 val 6
write i 7 key 7 val 7
write i 8 key 8 val 8
write i 9 key 9 val 9
write i 10 key 10 val 10
write i 11 key 11 val 11
write i 12 key 12 val 12
write i 13 key 13 val 13
write i 14 key 14 val 14
write i 15 key 15 val 15

read i 0 key 0 val 0
read i 1 key 1 val 1
read i 2 key 2 val 2
read i 3 key 3 val 3
read i 4 key 4 val 4
read i 5 key 5 val 5
read i 6 key 6 val 6
read i 7 key 7 val 7
read i 8 key 8 val 8
read i 9 key 9 val 9
read i 10 key 10 val 10
read i 11 key 11 val 11
read i 12 key 12 val 12
read i 13 key 13 val 13
read i 14 key 14 val 14
read i 15 key 15 val 15
```



## License

MIT License

Copyright (c) 2022 Hong Jiahua

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.